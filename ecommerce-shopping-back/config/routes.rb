Rails.application.routes.draw do
  resources :managers
  resources :products
  resources :carts
  resources :orders
  resources :users
  resources :comments
  resources :adresses
  resources :sales
  resources :categories
  resources :stocks
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
