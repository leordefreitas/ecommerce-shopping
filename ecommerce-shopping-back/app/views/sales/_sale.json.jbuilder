json.extract! sale, :id, :data, :total, :produtos, :created_at, :updated_at
json.url sale_url(sale, format: :json)
