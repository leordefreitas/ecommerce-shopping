class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.float :total
      t.string :status
      t.float :frete
      t.string :carrinho
      t.string :forma_pagamento

      t.timestamps
    end
  end
end
