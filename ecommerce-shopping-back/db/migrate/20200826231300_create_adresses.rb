class CreateAdresses < ActiveRecord::Migration[5.2]
  def change
    create_table :adresses do |t|
      t.string :cep
      t.string :estado
      t.string :bairro
      t.string :cidade
      t.string :nome
      t.string :rua
      t.string :complemento
      t.integer :numero
      t.string :tipo

      t.timestamps
    end
  end
end
