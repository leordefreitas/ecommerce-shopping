class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.string :nome
      t.string :imagem
      t.string :descricao
      t.string :categoria
      t.float :preco
      t.integer :likes
      t.float :peso
      t.float :dimensao

      t.timestamps
    end
  end
end
