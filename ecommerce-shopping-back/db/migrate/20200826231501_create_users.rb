class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :email
      t.string :senha
      t.string :tipo
      t.string :nome
      t.string :sobrenome
      t.string :telefone_fixo
      t.string :telefone_movel

      t.timestamps
    end
  end
end
