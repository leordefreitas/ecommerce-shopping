import React from 'react';
import './PesquisaProdutos.css';

function PesquisaProdutos() {
  return (
    <div className="pesquisa-produtos">
      <div className="pesquisa-1">
        <label>Digite um produto:  
          <input className="pesquisa-input" type="text"/>
        </label>
        <label>Maior Preço: 
          <input className="preco" type="text"/>
        </label>
      </div>
      <div className="pesquisa-2">
        <label>Categoria:
          <select className="select" name="select">
            <option value="">Selecione</option>
            <option value="eletronico">Eletrônico</option>
            <option value="acessorio">Acessórios</option>
          </select>
        </label>
        <label>Menor Preço: 
          <input className="preco" type="text"/>
        </label>
      </div>
      <div id="bto-produtos">
        <button className="bto-produtos">Pesquisar</button>
      </div>
    </div>
  )
}

export default PesquisaProdutos;
