import React from 'react';
import './ModalLogin.css';

function ModalLogin() {
  return (
    <div className="modal" id="modal-login">
      <div>
      <a href="#fechar" title="Fechar" className="fechar">X</a>
        <h2>Fazer Login</h2>
        <p>Nome</p>
        <input />
        <p>Senha</p>
        <input type="password"/>
        <button>Entrar</button>
      </div>
    </div>
  )
}

export default ModalLogin;
