import React from 'react';
import './EditarPerfil.css';
import InputEditarPerfil from '../../components/inputEditarPerfil/InputEditarPerfil';

function EditarPerfil() {
  return (
    <div>
      <InputEditarPerfil />
    </div>
  )
}

export default EditarPerfil;
