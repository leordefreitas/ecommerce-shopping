import React from 'react';
import './Produtos.css';
import PesquisaProdutos from '../../components/pesquisaProdutos/PesquisaProdutos';
import ProdutoGeral from '../../components/produtoGeral/ProdutoGeral';

function Produtos() {
  return (
    <div className="pagina-produtos">
      <h2>Produtos</h2>
      <PesquisaProdutos />
      <hr />
      <div className="lista-produtos">
        <ProdutoGeral />
        <ProdutoGeral />
        <ProdutoGeral />
      </div>
      <div className="lista-produtos">
        <ProdutoGeral />
        <ProdutoGeral />
        <ProdutoGeral />
      </div>
      <div className="mudando-pagina">
      </div>    
    </div>
  )
}

export default Produtos;
