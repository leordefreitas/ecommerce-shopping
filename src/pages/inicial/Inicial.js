import React from 'react';
import './Inicial.css';

// secoes
import SecaoChamativa from '../../components/secaoChamativa/SecaoChamativa';
import SecaoProdutosVendidos from '../../components/secaoProdutosVendidos/SecaoProdutosVendidos';
import SecaoSobre from '../../components/secaoSobre/SecaoSobre';

function Inicial() {
  return (
    <div className="pgn-inicial">
      <SecaoChamativa />
      <SecaoProdutosVendidos />
      <SecaoSobre />
    </div>
  )
}

export default Inicial;
