import React from 'react';
import { 
  BrowserRouter as Router,
  Route,
  Switch
} from 'react-router-dom';
import './App.css';

// paginas
import Inicial from './pages/inicial/Inicial';
import Produtos from './pages/produtos/Produtos';
import EditarPerfil from './pages/editarPerfil/EditarPerfil';
import NaoEncontrada from './pages/naoEncontrada/NaoEncontrada';
// componentes
import Header from './components/header/Header';
import Footer from './components/footer/Footer';
import ModalLogin from './components/modalLogin/ModalLogin';
import ModalCadastro1 from './components/modalCadastro1/ModalCadastro1';
import ModalCadastro2 from './components/modalCadastro2/ModalCadastro2';
import ModalCadastro3 from './components/modalCadastro3/ModalCadastro3';

function App() {
  return (
    <div className="App">
      <Router>
        <Header />
        <ModalLogin />
        <ModalCadastro1 />
        <ModalCadastro2 />
        <ModalCadastro3 />
        <Switch>
          <Route exact path="/"><Inicial /></Route>
          <Route exact path="/produtos"><Produtos /></Route>
          <Route exact path="/editarperfil"><EditarPerfil /></Route>
          <Route path="*"><NaoEncontrada /></Route>
        </Switch>
        <Footer />
      </Router>
    </div>
  );
}

export default App;
